package com.company.widget.model;

import com.company.widget.exception.BadRequestException;
import java.time.LocalDateTime;
import java.util.Map;

public class Widget {
    private int id;
    private int x;
    private int y;
    private int z;
    private int width;
    private int height;
    private LocalDateTime updatedAt;

    private static int idWidget = 1;

    public Widget(int x, int y, int z, int width, int height) {
        this.id = idWidget++;
        this.x = x;
        this.y = y;
        this.z = z;
        this.width = width;
        this.height = height;
        this.updatedAt = LocalDateTime.now();
    }

    public Widget(Map<String, Integer> widgetValue) {
        this.id = idWidget++;
        setX(widgetValue, true);
        setY(widgetValue, true);
        setZ(widgetValue, true);
        setWidth(widgetValue, true);
        setHeight(widgetValue, true);
        this.updatedAt = LocalDateTime.now();
    }

    public int getId() {
        return id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setX(int x) {
        this.x = x;
    }
    public void setX(Map<String, Integer> widgetValue) {
        if (widgetValue.containsKey("x")) {
            this.x = widgetValue.get("x");
        }
    }
    public void setX(Map<String, Integer> widgetValue, boolean creation) {
        if (widgetValue.containsKey("x")) {
            this.x = widgetValue.get("x");
        } else {
            throw new BadRequestException();
        }
    }

    public void setY(int y) {
        this.y = y;
    }
    public void setY(Map<String, Integer> widgetValue) {
        if (widgetValue.containsKey("y")) {
            this.y = widgetValue.get("y");
        }
    }
    public void setY(Map<String, Integer> widgetValue, boolean creation) {
        if (widgetValue.containsKey("y")) {
            this.y = widgetValue.get("y");
        } else {
            throw new BadRequestException();
        }
    }

    public void setZ(int z) {
       this.z = z;
    }
    public void setZ(Map<String, Integer> widgetValue) {
        if (widgetValue.containsKey("z")) {
            this.z = Repository.getValueZ(widgetValue.get("z"));
        }
    }
    public void setZ(Map<String, Integer> widgetValue, boolean creation) {
        if (widgetValue.containsKey("z")) {  // if a Z-index is specified
            this.z = Repository.getValueZ(widgetValue.get("z"));
        } else {  // if a Z-index not specified, the widget moves to the foreground
            this.z = Repository.getValueZ();
        }
    }

    public void setWidth(int width) {
        this.width = width;
    }
    public void setWidth(Map<String, Integer> widgetValue) {
        if (widgetValue.containsKey("width")) {
            this.width = widgetValue.get("width");
        }
    }
    public void setWidth(Map<String, Integer> widgetValue, boolean creation) {
        if (widgetValue.containsKey("width")) {
            this.width = widgetValue.get("width");
        } else {
            throw new BadRequestException();
        }
    }

    public void setHeight(int height) {
        this.height = height;
    }
    public void setHeight(Map<String, Integer> widgetValue) {
        if (widgetValue.containsKey("height")) {
            this.height = widgetValue.get("height");
        }
    }
    public void setHeight(Map<String, Integer> widgetValue, boolean creation) {
        if (widgetValue.containsKey("height")) {
            this.height = widgetValue.get("height");
        } else {
            throw new BadRequestException();
        }
    }

    public void setUpdatedAt() {
        this.updatedAt = LocalDateTime.now();
    }

}

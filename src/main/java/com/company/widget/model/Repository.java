package com.company.widget.model;

import com.company.widget.exception.NotFoundException;
import java.util.*;

public class Repository {
    private static ArrayList<Widget> widgets = new ArrayList<>();

    /**
     * Getting a list of widgets.
     * In the response we get a list of all widgets
     * sorted by z-index, from smallest to largest.
     *
     * @return list of all widgets
     */
    public ArrayList<Widget> getAll() {
        widgets.sort(Comparator.comparingInt(Widget::getZ));
        return widgets;
    }

    /**
     * Getting a widget by its identifier.
     * In the response we get a complete description of the widget.
     *
     * @param id widget identifier
     * @return widget description
     */
    public Widget getOneById(int id) {
        return widgets.stream()
                .filter(widget -> widget.getId() == id)
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }

    /**
     * Creating a widget.
     * Having set coordinates, z-index, width and height,
     * we get a complete widget description in the response.
     *
     * @param widgetValue array of values (x, y, z, width, height)
     * @return widget description
     */
    public Widget addWidget(Map<String, Integer> widgetValue) {
        Widget widget = new Widget(widgetValue);
        widgets.add(widget);
        return widget;
    }

    /**
     * Determining the z-index value.
     * If the existing Z-index is specified,
     * then the new widget shifts all widgets with the same and greater index upwards.
     *
     * @param newZ z-index value
     * @return z-index value
     */
    public static int getValueZ(int newZ) {
        if (widgets.stream().anyMatch(widget -> widget.getZ() == newZ)) {
            // shift all widgets with the same  and greater Z-index upwards
            widgets.stream()
                    .filter(widget -> widget.getZ() >= newZ)
                    .forEach(widget -> widget.setZ(widget.getZ() + 1));
        }
        return newZ;
    }

    /**
     * Determining the z-index value.
     * If a z-index is not specified,
     * the widget moves to the foreground
     *
     * @return z-index value
     */
    public static int getValueZ() {
        int newZ = widgets.stream()
                .mapToInt(Widget::getZ)
                .max()
                .orElse(0) +1;
        return newZ;
    }

    /**
     * Change widget data by its identifier.
     * In the response we get an updated full description of the widget.
     *
     * @param id widget identifier
     * @param widgetValue array of values (x, y, z, width, height)
     * @return widget description
     */
    synchronized public Widget updateWidget(int id, Map<String, Integer> widgetValue) {
        Widget widget = getOneById(id);

        widget.setX(widgetValue);
        widget.setY(widgetValue);
        widget.setZ(widgetValue);
        widget.setWidth(widgetValue);
        widget.setHeight(widgetValue);
        widget.setUpdatedAt();

        return widget;
    }

    /**
     * Deleting a widget. We can delete the widget by its identifier.
     *
     * @param id widget identifier
     */
    public void deleteWidget(int id) {
        Widget widget = getOneById(id);
        widgets.remove(widget);
    }
}

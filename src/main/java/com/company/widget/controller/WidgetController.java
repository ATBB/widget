package com.company.widget.controller;

import com.company.widget.model.Repository;
import com.company.widget.model.Widget;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;


@RestController
@RequestMapping("widget")
public class WidgetController {
    Repository repository = new Repository();

    @GetMapping
    public ArrayList<Widget> getAll() {
        return repository.getAll();
    }

    @GetMapping("{id}")
    public Widget getOne(@PathVariable int id) {
        return repository.getOneById(id);
    }

    @PostMapping
    public Widget create(@RequestBody Map<String, Integer> widgetValue) {
        return repository.addWidget(widgetValue);
    }

    @PutMapping("{id}")
    public Widget update(@PathVariable int id,
                         @RequestBody Map<String, Integer> widgetValue) {
        return repository.updateWidget(id, widgetValue);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable int id) {
        repository.deleteWidget(id);
    }
}

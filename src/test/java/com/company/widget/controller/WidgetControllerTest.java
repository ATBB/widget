package com.company.widget.controller;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class WidgetControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WidgetController controller;

    private String url = "/widget";

    /**
     * Checking controller answer
     * @throws Exception
     */
    @Test
    @Order(1)
    void availabilityTest() throws Exception {
        this.mockMvc.perform(get(this.url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * Creating a widget.
     * Having set coordinates, z-index, width, and height,
     * we get a complete widget description in the response.
     * The server generates the identifier
     * @throws Exception
     */
    @Test
    @Order(2)
    void createTest() throws Exception {
        String body1 = "{\"x\":\"11\",\"y\":\"11\",\"z\":\"11\",\"width\":\"30\",\"height\":\"40\"}";

        this.mockMvc.perform(post(this.url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body1))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{'id':1, 'x':11, 'y': 11, 'z': 11, 'width': 30, 'height': 40}"));

        String body2 = "{\"x\":\"12\",\"y\":\"12\",\"z\":\"12\",\"width\":\"50\",\"height\":\"60\"}";

        this.mockMvc.perform(post(this.url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body2))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{'id':2, 'x':12, 'y':12, 'z': 12, 'width': 50, 'height': 60}"));
    }

    /**
     * Getting a list of widgets.
     * In the response we get a list of all widgets sorted by z-index, from smallest to largest.
     * @throws Exception
     */
    @Test
    @Order(3)
    void getAllTest() throws Exception {
        this.mockMvc.perform(get(this.url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*").isArray())
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value("1"))
                .andExpect(jsonPath("$[0].x").value("11"))
                .andExpect(jsonPath("$[0].z").value("11"))
                .andExpect(jsonPath("$[1].id").value("2"))
                .andExpect(jsonPath("$[1].x").value("12"))
                .andExpect(jsonPath("$[1].z").value("12"));
    }


    /**
     * Getting a widget by its identifier.
     * In the response we get a complete description of the widget.
     * @throws Exception
     */
    @Test
    @Order(4)
    void getOneTest() throws Exception {
        this.mockMvc.perform(get(this.url + "/2"))
                .andExpect(status().isOk())
                .andExpect(content().json("{'id':2, 'x':12, 'y':12, 'z':12, 'width': 50, 'height': 60}"));
    }

    /**
     * Updating a widget.
     * Change widget data by its identifier.
     * In the response we get an updated full description of the widget.
     * You cannot change widget id.
     * You cannot delete widget attributes.
     * @throws Exception
     */
    @Test
    @Order(5)
    void updateTest() throws Exception {
        String body1 = "{\"x\":\"10\",\"y\":\"10\"}";

        this.mockMvc.perform(put(this.url + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body1))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{'id':1, 'x':10, 'y':10, 'z':11, 'width': 30, 'height': 40}"));
    }

    /**
     * Updating a widget.
     * Check if the existing z-index is specified,
     * then the new widget shifts all widgets
     * with the same and greater index upwards.
     * @throws Exception
     */
    @Test
    @Order(6)
    void updateValueZTest() throws Exception {
        // the existing z-index is specified
        String body1 = "{\"z\":\"12\"}";

        this.mockMvc.perform(put(this.url + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(body1))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{'id':1, 'x':10, 'y':10, 'z':12}"));

        // checking the shift all widgets with the same and greater index upwards
        this.mockMvc.perform(get(this.url + "/2"))
                .andExpect(status().isOk())
                .andExpect(content().json("{'id':2, 'x':12, 'y':12, 'z':13}"));
    }

    /**
     * Creating a widget.
     * If a z-index is not specified,
     * the widget moves to the foreground
     * @throws Exception
     */
    @Test
    @Order(7)
    void createWithoutZTest() throws Exception {
        String body1 = "{\"x\":\"14\",\"y\":\"14\",\"width\":\"70\",\"height\":\"80\"}";

        this.mockMvc.perform(post(this.url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body1))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{'id':3, 'x':14, 'z':14}"));
    }

    /**
     * Creating a widget.
     * If x is not specified or is not integer,
     * throw an exception
     * @throws Exception
     */
    @Test
    @Order(8)
    void createWrongTest() throws Exception {
        String body1 = "{\"y\":\"11\",\"z\":\"11\",\"width\":\"30\",\"height\":\"40\"}";

        this.mockMvc.perform(post(this.url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body1))
                .andDo(print())
                .andExpect(status().isBadRequest());

        String body2 = "{\"x\":\"str\",\"y\":\"12\",\"z\":\"12\",\"width\":\"50\",\"height\":\"60\"}";

        this.mockMvc.perform(post(this.url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(body2))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    /**
     * Deleting a widget.
     * We can delete the widget by its identifier.
     * @throws Exception
     */
    @Test
    @Order(9)
    void deleteTest() throws Exception {
        String urlToDelete = this.url + "/1";
        this.mockMvc.perform(delete(urlToDelete))
                .andDo(print())
                .andExpect(status().isOk());

        // Attempt to get by ID
        this.mockMvc.perform(get(urlToDelete))
                .andExpect(status().isNotFound());
    }
}